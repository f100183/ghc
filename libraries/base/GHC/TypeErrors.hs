{-# LANGUAGE DataKinds #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module GHC.TypeErrors (
  TypeError,
  ErrorMessage(..),
  ConstraintFailure(..),
  CustomError(..),
  CustomErrorAlternative(..),
  type (:=>:),
  (:~?:), (:~!:),
  CustomErrors,
  WhenApart,
  WhenLeftUndischarged,
  ScheduleAtTheEnd
  ) where

import GHC.Types
import GHC.TypeLits

type WhenApart a b no yes = WhenApartUnsafe a b no (a ~ b, yes)

infixl 5 :/~:
data {-kind-} ConstraintFailure
  = forall t. t :/~: t
  | Undischarged Constraint

infixl 4 :=>:, :=?>:  -- Bind less than ErrorMessage and :/~:
data {-kind-} CustomError
  = ConstraintFailure :=?>: ([CustomErrorAlternative], ErrorMessage)
  | Check Constraint
type cs :=>: msg = cs ':=?>: '( '[], msg )

infixl 4 :=!>:
data {-kind-} CustomErrorAlternative
  = Constraint :=!>: ErrorMessage

infixl 5 :~?:, :~!:
class (a ~ b) => a :~?: b
class (a ~ b) => a :~!: b

data {-kind-} CustomErrorsPath
  = CEP_Ok | CEP_Fail

type family CustomErrors css :: Constraint where
  CustomErrors '[]       = ()
  CustomErrors (c ': cs) = CustomErrors' 'CEP_Ok c cs

type family CustomErrors' p cs rest :: Constraint where
  CustomErrors' 'CEP_Ok   '[] rest = CustomErrors rest
  CustomErrors' 'CEP_Fail '[] rest = ()
  CustomErrors' p ((a ':/~: b ':=?>: '(alts, msg)) ': cs) rest
    = WhenApart a b (ScheduleAtTheEnd
                       (CustomErrorsAlternative alts msg
                          (CustomErrors' 'CEP_Fail cs '[])))
                    (CustomErrors' p cs rest)
  CustomErrors' p (('Undischarged c ':=?>: '(alts, msg)) ': cs) rest
    = (WhenLeftUndischarged c msg, CustomErrors' p cs rest)
  CustomErrors' p (('Check c) ': cs)                  rest
    = (c, CustomErrors' p cs rest)

type family CustomErrorsAlternative alts fail rest :: Constraint where
  CustomErrorsAlternative '[] fail rest = (TypeError fail, rest)
  CustomErrorsAlternative (((a :~?: b) ':=!>: msg) ': cs) fail rest
    = WhenApartUnsafe a b (CustomErrorsAlternative cs fail rest) (TypeError msg, rest)
  CustomErrorsAlternative (((a :~!: b) ':=!>: msg) ': cs) fail rest
    = WhenApart a b (CustomErrorsAlternative cs fail rest) (TypeError msg, rest)