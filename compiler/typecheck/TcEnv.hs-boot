{-
>module TcEnv where
>import TcRnTypes
>
>tcExtendIdEnv :: [TcId] -> TcM a -> TcM a
-}

module TcEnv where

import Name      (Name)
import TyCon     (TyCon)
import TcRnTypes (TcM)
import InstEnv   (InstEnvs)

tcLookupTyCon :: Name -> TcM TyCon
tcGetInstEnvs :: TcM InstEnvs