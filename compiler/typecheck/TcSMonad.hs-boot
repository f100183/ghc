module TcSMonad (TcS, runTcS) where

import TcRnTypes  (TcM)
import TcEvidence (EvBindMap)

data TcSEnv
newtype TcS a = TcS { unTcS :: TcSEnv -> TcM a }
runTcS :: TcS a -> TcM (a, EvBindMap)