module TcFlatten where

import TcType (TcType)
import TcEvidence (TcCoercion)
import TcRnTypes (Xi, CtEvidence)
import {-# SOURCE #-} TcSMonad (TcS)

data FlattenMode = FM_FlattenAll
                 | FM_SubstOnly
             --  | FM_Avoid TcTyVar Bool

flatten :: FlattenMode -> CtEvidence -> TcType
        -> TcS (Xi, TcCoercion)